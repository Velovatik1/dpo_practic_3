import pygame
import time

pygame.init()
screen = pygame.display.set_mode((400, 500))
done = False

dt = 0.01
t = 0
G = 9.8
vy0 = 10

vy = 0
sy0 = 0
sy = 0


def calc_vy(vy0, G, t):
    return vy0 + G * t


def calc_sy(vy0, G, t):
    return vy0 * t - (G * t ** 2) / 2


while not done:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

        while sy >= 0:
            vy = calc_vy(vy0, G, t)  # Calculate speed
            sy = calc_sy(vy0, G, t)  # Calculate coordinate in y axis

            print("Speed: ", vy, " Coordinate: ", sy)

            screen.fill((0, 0, 0))  # Erase previous rect

            pygame.draw.rect(screen, (0, 255, 0), pygame.Rect(50, 500 - sy * 100, 10, 10))

            pygame.display.flip()

            t += dt

            time.sleep(dt)
